const express =require("express");
const app = express();
const logger=require('morgan');
const dotenv = require("dotenv");
const path= require("path");
var cors = require("cors");
var bodyParser = require('body-parser');
var createError = require('http-errors');

dotenv.config({path:"./config/config.env"});

const PORT = process.env.PORT||5000;

app.use(logger('dev'));
app.use(express.json());
app.use(bodyParser.json({ type: 'application/*+json' }))
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public/dist/configurateur')));
app.set('views', __dirname + '/views')
app.set('view engine', 'ejs');
app.engine('ejs', require('ejs').__express);
app.use(cors())

app.get('/*',(req,res)=>{
  res.sendFile('index.html',{root:'./dist/configurateur'});
});

app.listen(PORT,()=>{
  console.log(`Server started at : ${PORT}`);
})
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});



