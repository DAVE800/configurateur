FROM node:latest
WORKDIR /src

COPY . /src

RUN npm update npm
RUN npm install --production

EXPOSE 4000

CMD ["node", "app.js"]
